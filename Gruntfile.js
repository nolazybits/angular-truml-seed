/**
 * Created by nolazybits on 16/02/15.
 */

module.exports = function(grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    var modules = [
        'core'
    ];

    var libraries = [
        'angular/angular.js',
        'angular-animate/angular-animate.js',
        'angular-aria/angular-aria.js',
        'angular-material/angular-material.js',
        'angular-material/angular-material.css',
        'angular-ui-router/release/angular-ui-router.js',
        'jquery/dist/jquery.js',
        'oclazyload/dist/ocLazyLoad.js',
        'requirejs/require.js',
        'requirejs-domready/domReady.js',
        'requirejs-text/text.js'
    ];

    var libraries_dev = [
        'angular-mocks/angular-mocks.js'
    ]


    grunt.initConfig({
        //pkg: grunt.file.readJSON('package.json'),

        // Project settings
        application: {
            src :  'app',
            dist:  'dist'
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                options: {
                    force: true
                },
                files: [{
                    dot: true,
                    src: [
                        '<%= application.dist %>'
                    ]
                }]
            }
        },

        // Compiles Sass to CSS using COMPASS framework and generates necessary files if requested
        compass: {
            options: {
                sassDir: '<%= application.src %>/',
                cssDir: '<%= application.dist %>/',
                imagesDir: ['<%= application.src %>/'],
                generatedImagesDir: '<%= application.dist %>/',
                //javascriptsDir: '<%= application.src %>',
                //fontsDir: '<%= application.src %>',
                importPath: './bower_components',
                //httpImagesPath: '../images',
                //httpGeneratedImagesPath: '../images',
                httpFontsPath: 'fonts',
                relativeAssets: false,
                assetCacheBuster: false,
                raw: 'Sass::Script::Number.precision = 10\n'
            },
            dev: {
                options: {
                    debugInfo: true
                }
            },
            dist: {
            }
        },

        //  transcript any typescript files
        ts: {
            options: {
                basePath: '<%= application.src %>',
                target: 'es5',
                sourceMap: false
            },
            common: {
                src: '<%= application.src %>/**/{,}*.ts',
                dest: '<%= application.dist %>/'
            }
        },

        //  copy html / php files to dev folder
        copy: {
            //  copy any html files and images
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= application.src %>',
                    dest: '<%= application.dist %>',
                    src: [
                        '.htaccess',
                        '**/*.html',
                        '**/*.php',
                        '**/*.css'
                    ]
                }]
            },
            images: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= application.src %>',
                    dest: '<%= application.dist %>',
                    src: [
                        '**/*.{ico,png,svg}',
                        '!**/to_sprite'
                    ]
                }]
            },
            libraries: {
                files: [{
                    // Enable dynamic expansion.
                    expand: true,
                    // Src matches are relative to this path.
                    cwd: 'bower_components/',
                    // Actual pattern(s) to match.
                    src: libraries,
                    // Destination path prefix.
                    dest: '<%= application.dist %>/libs/',
                    // Dest filepaths will have this extension.
                    //ext: '.min.js',
                    extDot: 'last',
                    rename: function (dest, src) {
                        var a = src.split(".");
                        if( a.length !== 1 || ( a[0] !== "" && a.length > 2 ) ) {
                            var ext = a.pop();
                            if( ext === 'js' )
                            {
                                return dest + src.replace('.js', '.min.js');
                            }
                        }
                        return dest + src;
                    }
                }]
            },
            'libraries-dev': {
                files: [{
                    // Enable dynamic expansion.
                    expand: true,
                    // Src matches are relative to this path.
                    cwd: 'bower_components/',
                    // Actual pattern(s) to match.
                    src: libraries_dev,
                    // Destination path prefix.
                    dest: '<%= application.dist %>/libs/',
                    // Dest filepaths will have this extension.
                    //ext: '.min.js',
                    extDot: 'last',
                    rename: function (dest, src) {
                        var a = src.split(".");
                        if( a.length !== 1 || ( a[0] !== "" && a.length > 2 ) ) {
                            var ext = a.pop();
                            if( ext === 'js' )
                            {
                                return dest + src.replace('.js', '.min.js');
                            }
                        }
                        return dest + src;
                    }
                }]
            }
        }
    });

    grunt.registerTask('runTaskOnModules', 'This will apply the task on all the sub modules', function(taskName, type) {
        //grunt.log.warn(subProjects.length);
        for (var i = 0; i< modules.length; i++ )
        {
            grunt.log.writeln();
            grunt.log.ok('Executing task: '+ taskName +':'+ type +':'+ modules[i]);
            grunt.task.run(''+ taskName +':'+ type +':'+ modules[i]);
        }
    });

    grunt.registerTask('build', [
        'clean',
        'compass:dev',
        'ts:common',
        'copy:html',
        'copy:images',
        'copy:libraries'
    ]);

    grunt.registerTask('build-dev', [
        'build',
        'copy:libraries-dev'
    ]);

    grunt.registerTask('build-dist', [
        'build'
    ]);

    grunt.registerTask('default', 'build-dev');
};