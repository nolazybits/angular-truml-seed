## What is this repository for? ##

**Description**  
Seed project for AngularJS, RequireJS, Typescript and Angular Material

**Version:**  
1.0.0

## How do I get set up? ##

### Installing tools ###

#### Tools to build ####
**NodeJS and npm**  
NodeJS is a dependency manager for servers.  
Check the Nodejs [official website](https://nodejs.org/) for help.
Once NodeJS and npm (Node Package Manager) are installed you can do:
`npm install`
which will install all the dependencies to build our project

**Ruby and Compass**  
Compass is a CSS Authoring Framework.  
Check the official [compass website](http://compass-style.org/) for help.  
Once installed ruby for your system run  
`$ sudo gem update`  
`$ sudo gem install compass`  

**Grunt**   
Grunt is a task manager.  
Check the grunt [official website](http://gruntjs.com) for help.  
`$ npm install -g grunt-cli`

**Bower**  
Bower is a dependency manager for javascript library used in the browser.  
Check the [official website](http://bower.io) for help.  
`$ npm install -g bower`
then you can install the libraries used by our project (ie loaded by the browser when running the application) using:
`bower install`

**Typescript**  
TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.
Check the [official website](http://www.typescriptlang.org/) for help.
`$ npm install -g typescript`
Once the typescript transpiller is installer you will need to get the TypeScript Definitions from
[DefinitelyTyped Github Repo](https://github.com/borisyankov/DefinitelyTyped).
A tool has been created to download those one automatically for us reading the tsd.json in the base folder of this
project.
Please use the command
`$ tsd reinstall -s`  
to download all the necessary tsd files.

#### Tools to Test ####

### Running Grunt Tasks ###

#### Building ####
Now that the build tools are set up you can now build the project using either:  

*  for development  
`$ grunt build`  
*  for production  
`$ grunt build-dist`

you will then find a *`dist`* folder

#### Testing ####
TODO

#### Deployment ####
TODO

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact

## Links to sort ##
Angular Best Practice recommendataions
https://docs.google.com/document/d/1XXMvReO8-Awi1EZXAXS4PzDzdNvV6pGcuaF4Q9821Es/pub

AngularJS, Typescript, RequireJS
https://github.com/adamholdenyall/adam-holden.com-TypeScript_Tutorials/blob/master/part1/game.html

Browserify Handbook
https://github.com/substack/browserify-handbook#external-bundles

Browserify Posts
https://ampersandjs.com/learn/npm-browserify-and-modules

Interresting transforms (plugins)
browserify-css
https://www.npmjs.com/package/parcelify
https://github.com/epeli/browserify-externalize
https://github.com/substack/factor-bundle
https://www.npmjs.com/package/partition-bundle

Full list at https://github.com/substack/node-browserify/wiki/list-of-transforms


Angular and typescript
https://github.com/seanhess/angularjs-typescript
http://piotrwalat.net/using-typescript-with-angularjs-and-web-api/
http://nikosbaxevanis.com/blog/2014/04/03/typescript-slash-grunt-slash-angular/
https://github.com/bestander/webpack-typescript-angular
https://github.com/lindem/Angular-Browserify-TypeScript-Example

Angular UI Router
https://github.com/angular-ui/ui-router/wiki/Nested-States-%26-Nested-Views

Browserify
http://www.sitepoint.com/getting-started-browserify/
Browserify typecript typeify
http://benclinkinbeard.com/posts/external-bundles-for-faster-browserify-builds/
http://aeflash.com/2014-03/a-year-with-browserify.html
http://mherman.org/blog/2014/08/14/kickstarting-angular-with-gulp/#.VOKrxnWUf0p
https://github.com/basti1302/angular-browserify

Browserify vs Webpack
http://blog.namangoel.com/browserify-vs-webpack-js-drama

RaptorJS from ebay seems great

Large JS application
http://enterprisewebbook.com/ch6_large_js_apps.html

Lazy Loading in Browserify
https://www.npmjs.com/package/factor-bundle
https://github.com/substack/browserify-handbook#partition-bundle

Require vs Browserify for Angular
http://developer.telerik.com/featured/requiring-vs-browerifying-angular/
https://medium.com/@dickeyxxx/best-practices-for-building-angular-js-apps-266c1a4a6917
https://lincolnloop.com/blog/speedy-browserifying-multiple-bundles/