/**
 * The main application
 *
 * @type {ng.IModule}
 */
import adminModule = require('./admin-module'); adminModule;
import adminConfig = require('./admin-config'); adminConfig;
import adminRoutes = require('./admin-routes'); adminRoutes;

export var name = adminModule.name;