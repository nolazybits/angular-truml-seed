/**
 * The main application
 *
 * @type {ng.IModule}
 */

'use strict';
import angular = require('angular'); angular;
import admin = require('./admin-module'); admin;

admin.module.config(config);

config.$inject = ['$locationProvider', '$controllerProvider'];
function config($locationProvider: ng.ILocationProvider, $controllerProvider: ng.IControllerProvider)
{
    $locationProvider.html5Mode(true);
}
