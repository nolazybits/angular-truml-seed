/**
 * The main application
 *
 * @type {ng.IModule}
 */

import angular = require('angular'); angular;

'use strict';

export var name = 'app.admin';
export var url = '/admin';
export var path = '/admin';
export var module = angular.module(name, []);

