/**
 * The main application
 *
 * @type {ng.IModule}
 */

import angular = require('angular'); angular;
import admin = require('./admin-module'); admin;

'use strict';

admin.module.config(config);

config.$inject = ['$stateProvider', '$urlRouterProvider'];
function config($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider)
{
    $stateProvider
    .state('admin', {
        url: admin.url + '/',
        data: {
            //  this property will apply to all children of 'admin'
            requireLogin: true
        },
        views: {
            'navigation@': {
                templateUrl: admin.path + '/admin.menu.html'
            },
            'content@': {
                //template: 'this is the default template for admin<div ui-view="content"></div>'
                templateUrl: admin.path + '/admin.content.html'
            }
        }
    });
}

import feature = require('./feature/admin-feature-route'); feature;
import another = require('./another/admin-another-route'); another;