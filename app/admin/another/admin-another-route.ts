/**
 * The main application
 *
 * @type {ng.IModule}
 */
import angular = require('angular'); angular;
import admin = require('../admin-module'); admin;

import AdminAnotherController = require('./AdminAnotherController'); AdminAnotherController;

'use strict';

admin.module.config(config);

config.$inject = ['$stateProvider', '$urlRouterProvider'];
function config($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider)
{
    $stateProvider
    .state('admin.another', {
        url: 'another/',
        views: {
            'content@admin': {
                templateUrl: admin.url + '/another/admin.another.html',
                controller: AdminAnotherController.name,
                controllerAs: 'anotherCtrl'
            }
        }
    });
}