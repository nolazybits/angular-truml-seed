import admin = require('../admin-module');

'use strict';

export var name:string = 'app.admin.AdminFeatureController';
export class AdminFeatureController {

    test:string;

    // $inject annotation.
    // It provides $injector with information about dependencies to be injected into constructor
    // it is better to have it close to the constructor, because the parameters must match in count and type.
    // See http://docs.angularjs.org/guide/di
    public static $inject = [
        '$scope'
    ];

    // dependencies are injected via AngularJS $injector
    // controller's name is registered in Application.ts and specified from ng-controller attribute in index.html
    constructor(private $scope:any) {
        this.test = "This text is coming from the controller";
    }
}

//  now add the controller to the angular module
admin.module.controller(name, AdminFeatureController);