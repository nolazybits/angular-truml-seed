/**
 * The main application
 *
 * @type {ng.IModule}
 */
import angular = require('angular'); angular;
import admin = require('../admin-module'); admin;

import AdminFeatureController = require('./AdminFeatureController'); AdminFeatureController;

'use strict';

admin.module.config(config);

config.$inject = ['$stateProvider', '$urlRouterProvider'];
function config($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider)
{
    $stateProvider
    .state('admin.feature', {
        url: 'feature/',
        views: {
            'content@admin': {
                templateUrl: admin.url + '/feature/admin.feature.html',
                controller: AdminFeatureController.name,
                controllerAs: 'featureCtrl'
            }
        }
    });
}