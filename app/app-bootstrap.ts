/**
 * The main application
 *
 * @type {ng.IModule}
 */
import appModule = require('./app-module'); appModule;
import appConfig = require('./app-config'); appConfig;

export var name:string = appModule.name;