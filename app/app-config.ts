/**
 * The main application
 *
 * @type {ng.IModule}
 */

'use strict';
import angular = require('angular'); angular;
import app = require('./app-module'); app;

app.module.config(config);

config.$inject = ['$locationProvider'];
function config($locationProvider: ng.ILocationProvider)
{
    $locationProvider.html5Mode(true);
}