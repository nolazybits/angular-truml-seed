/**
 * The main application
 *
 * @type {ng.IModule}
 */

import angular = require('angular'); angular;
import core = require('./core/core-bootstrap'); core;

import backendless = require('./backendless/backendless-bootstrap'); backendless;

'use strict';
export var name:string = 'app';
export var module = angular.module(name,
    [
        //  load all external modules needed to run the application
        core.name,
        backendless.name
    ]
);


