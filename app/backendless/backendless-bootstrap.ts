/**
 * The main application
 *
 * @type {ng.IModule}
 */
import backendlessModule = require('./backendless-module'); backendlessModule;
import backendlessRun = require('./backendless-run'); backendlessRun;

export var name = backendlessModule.name;