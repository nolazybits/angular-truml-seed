/**
 * The main application
 *
 * @type {ng.IModule}
 */

import angular = require('angular'); angular;
import mocks = require('angular-mocks'); mocks;

'use strict';

export var name = 'app.backendless';
export var module = angular.module(name, ['ngMockE2E']);

