/**
 * The main application
 *
 * @type {ng.IModule}
 */

'use strict';
import angular = require('angular'); angular;
import backendless = require('./backendless-module'); backendless;

backendless.module.run(run);

run.$inject = ['$httpBackend'];
function run($httpBackend)
{
    $httpBackend.whenGET('/auth/user').respond(AuthUserFail());

    $httpBackend.whenGET(/^.*/).passThrough();
}

function AuthUser() {
    return {
            userId: 1,
            userName: 'Mock User',
            avatarUrl: 'http://www.avatarist.com/avatars/Cartoons/4-Eyes.gif'
        };
}

function AuthUserFail() {
    return {"error":"not signin"};
}