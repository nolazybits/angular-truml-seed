/**
 * The main application
 *
 * @type {ng.IModule}
 */
import coreModule = require('./core-module'); coreModule;
import coreConfig = require('./core-config'); coreConfig;
import coreRoutes = require('./core-routes'); coreRoutes;
import coreRun = require('./core-run'); coreRun;

export var name = coreModule.name;