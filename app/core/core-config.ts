/**
 * The main application
 *
 * @type {ng.IModule}
 */

'use strict';
import angular = require('angular'); angular;
import core = require('./core-module'); core;

core.module.config(config);

config.$inject = ['$locationProvider', '$controllerProvider', '$mdIconProvider', '$ocLazyLoadProvider'];
function config($locationProvider: ng.ILocationProvider, $controllerProvider: ng.IControllerProvider, $mdIconProvider: angular.material.MDIconProvider, $ocLazyLoadProvider : oc.ILazyLoadProvider )
{
    //  set html5 mode
    $locationProvider.html5Mode(true);
    $ocLazyLoadProvider.config({
        asyncLoader: requirejs,
        debug: true
    });

    //  icon provider: link a set of icon to a svg sprite sheet. see
    $mdIconProvider
        .iconSet('navigation', 'core/main/images/icons/svg-sprite-navigation.svg');
}

//  import the core controller (navigation)
import navigationController = require('./main/scripts/NavigationController'); navigationController;

//  now import our features
import login = require('./login/core-login-bootstrap'); login;