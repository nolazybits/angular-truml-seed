/**
 * The main application
 *
 * @type {ng.IModule}
 */

import angular = require('angular'); angular;
import angularAnimate = require('angular-animate'); angularAnimate;
import angularAria = require('angular-aria'); angularAria;
import angularMaterial = require('angular-material'); angularMaterial;
import angularUIRouter = require('angular-ui-router'); angularUIRouter;
import ocLazyLoad = require('ocLazyLoad'); ocLazyLoad;

'use strict';

export var name = 'app.core';
export var url = '/core';
export var path = '/core';
export var module = angular.module('app.core', [
    'ngAria',
    'ngAnimate',
    'ngMaterial',
    'ui.router',
    'oc.lazyLoad'
]);

