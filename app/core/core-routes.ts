/**
 * The main application
 *
 * @type {ng.IModule}
 */

/*
    There are no routes but we are adding here the controllers needed for the core module
    The core controller is the controller registered against the main body.
    The NavigationController is registered against the section showing both the sidenav and the content.
 */

import angular = require('angular'); angular;
import core = require('./core-module'); core;

'use strict';

core.module.config(config);
config.$inject = ['$stateProvider', '$urlRouterProvider'];
function config($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider)
{
    //For deep linking
    //$urlRouterProvider.otherwise('/');
    $urlRouterProvider.otherwise(function($injector, $location) {
        var $state = $injector.get("$state");
        var futureState = $location.path()
            .replace(/^(\/)/,"")
            .replace(/(\/)$/,"")
            .split('/')
            .join('.');
        $state.go(futureState);
    });
}