/**
 * The main application
 *
 * @type {ng.IModule}
 */

'use strict';
import angular = require('angular'); angular;
import core = require('./core-module'); core;

core.module.run(run);

run.$inject = ['$rootScope','$state','$stateParams', '$ocLazyLoad'];
function run($rootScope, $state, $stateParams, $ocLazyLoad:oc.ILazyLoad)
{
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$ocLazyLoad = $ocLazyLoad;
    $rootScope.$on('$stateNotFound', stateNotFound);

}

/**
 * This function is being called when a state is not found, and will try to lazy load it.
 * @param event
 * @param unfoundState
 * @param fromState
 * @param fromParams
 */
function stateNotFound(event, unfoundState, fromState, fromParams)
{
    console.log("Configuring $stateNotFound.");
    console.log(unfoundState.to); // "lazy.state"
    console.log(unfoundState.toParams); // {a:1, b:2}
    console.log(unfoundState.options); // {inherit:false} + default options

    var self =  event.currentScope,
        toState = unfoundState.to,
        toStateParent = toState;

    if (toState.indexOf(".")) {
        toStateParent = toState.split(".")[0];
    }

    event.preventDefault();

    self.$ocLazyLoad.load(toStateParent +'/'+ toStateParent +'-bootstrap.js')
        .then(function() {
            console.log('Redirecting to '+ unfoundState.to);
            self.$state.go(unfoundState.to, unfoundState.toParams, unfoundState.options);
        }, function(e){
            //console.log('Couldn\'t load module '+ toStateParent +'/'+ toStateParent +'-bootstrap.js');
            //console.log(e);
        });
}