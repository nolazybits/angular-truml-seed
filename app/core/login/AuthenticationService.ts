import core = require('../core-module');

'use strict';

export var name:string = 'app.core.AuthenticationService';
export class AuthenticationService {

    userId      : number = null;
    userName    : string = null;
    avatarUrl   : string = null;

    // $inject annotation.
    // It provides $injector with information about dependencies to be injected into constructor
    // it is better to have it close to the constructor, because the parameters must match in count and type.
    // See http://docs.angularjs.org/guide/di
    public static $inject = [
        '$http',
        '$q',
        '$state',
    ];

    // dependencies are injected via AngularJS $injector
    // controller's name is registered in Application.ts and specified from ng-controller attribute in index.html
    constructor(private $http, private $q, private $state) {
    }

    isLoggedIn()  {
        var self = this;
        return this.$http.get('/auth/user')
        .then(function (response) {
            self.userId = response.data.userId;
            self.userName = response.data.userName;
            self.avatarUrl = response.data.avatarUrl;
            return {
                'userName'  : self.userName,
                'userId'    : self.userId,
                'avatarUrl' : self.avatarUrl
            };
        });
    }
}

//  now add the controller to the angular module
core.module.service(name, AuthenticationService);