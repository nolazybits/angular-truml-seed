import core = require('../core-module');

'use strict';

export var name:string = 'app.core.LoginController';
export class LoginController {

    // $inject annotation.
    // It provides $injector with information about dependencies to be injected into constructor
    // it is better to have it close to the constructor, because the parameters must match in count and type.
    // See http://docs.angularjs.org/guide/di
    public static $inject = [
        '$scope'
    ];

    // dependencies are injected via AngularJS $injector
    // controller's name is registered in Application.ts and specified from ng-controller attribute in index.html
    constructor(private $scope:any) {
    }

    cancel() {
        this.$scope.$dismiss;
    }

    submit (email, password) {
/*        UsersApi.login(email, password).then(function (user) {
            $scope.$close(user);
        });*/
    }
}

//  now add the controller to the angular module
core.module.controller(name, LoginController);