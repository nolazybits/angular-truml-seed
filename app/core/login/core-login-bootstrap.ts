/**
 * The main application
 *
 * @type {ng.IModule}
 */
import loginRoute = require('./core-login-route'); loginRoute;
import loginRun = require('./core-login-run'); loginRun;
import authenticationService = require('./AuthenticationService'); authenticationService;