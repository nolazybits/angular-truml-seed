/**
 * The main application
 *
 * @type {ng.IModule}
 */
import angular = require('angular'); angular;
import core = require('../core-module'); core;

import LoginController = require('./LoginController'); LoginController;

'use strict';

core.module.config(config);

config.$inject = ['$stateProvider', '$urlRouterProvider'];
function config($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider)
{
    $stateProvider
    .state('login', {
        url: 'login/',
        views: {
            'content@': {
                templateUrl: core.path + '/login/core.login.html',
                controller: LoginController.name,
                controllerAs: 'login'
            }
        }
    });
}