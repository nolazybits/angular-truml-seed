/**
 * The main application
 *
 * @type {ng.IModule}
 */
import angular = require('angular'); angular;
import core = require('../core-module'); core;
import AuthService = require('./AuthenticationService');

'use strict';

core.module.run(run);

var self = {
    authService: null,
    $state: null
};

run.$inject = ['$rootScope', AuthService.name, '$state' ];
function run($rootScope, authService, $state)
{
    self.authService = authService;
    self.$state = $state;
    $rootScope.$on('$stateChangeStart', stateChangeStart);
}


/**
 * Very basic authentication, authorization checks.
 * TODO to improve authentication and authorization checks
 * @param event
 * @param toState
 * @param toParams
 * @param fromState
 * @param fromParams
 */
function stateChangeStart (event, toState, toParams, fromState, fromParams) {
    var requireLogin = toState.data.requireLogin;
    if (requireLogin ) {
        self.authService.isLoggedIn()
        .then(function(userData) {
                if (! userData.userId ) {
                    self.$state.go('login');
                    event.preventDefault();
                }
                //else
                //{
                //    self.$state.transitionTo(toState, toParams);
                //}
        });

    }
}