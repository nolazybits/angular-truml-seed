import core = require('../../core-module'); core;

'use strict';

export class NavigationController {

    private _mdSidenav;
    private _log        : ng.ILogService;
    private _menu       : Array<String>;

    // $inject annotation.
    // It provides $injector with information about dependencies to be injected into constructor
    // it is better to have it close to the constructor, because the parameters must match in count and type.
    // See http://docs.angularjs.org/guide/di
    public static $inject = [
        '$scope',
        '$mdSidenav',
        '$log'
    ];

    // dependencies are injected via AngularJS $injector
    // controller's name is registered in Application.ts and specified from ng-controller attribute in index.html
    constructor(private $scope:ng.IScope, $mdSidenav, $log:ng.ILogService) {
        this._menu = [];
        this._mdSidenav = $mdSidenav;
        this._log = $log;
    }

    buildNavigation() {

    }

    toggleLeft() {
        this._mdSidenav('left').toggle()
            .then(function(){
                this.log.debug("toggle left is done");
            });
    }

    close() {
        this._mdSidenav('left').close()
            .then(function(){
               this.log.debug("left sidebar is closed");
            });
    }

    addMenu(menu:string) : void
    {
        console.log(menu);
    }

    get menu():Array<String>
    {
        return this.menu;
    }


}

//  now add the controller to the angular module
core.module.controller('app.core.NavigationController', NavigationController);