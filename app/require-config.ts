//  this is to defer angular bootsrap until everything is loaded.
//window.name = 'NG_DEFER_BOOTSTRAP!';

//  REQUIRE CONFIG AND ERROR HANDLING
require.config({

    enforceDefine: true,

    baseUrl: './',

    paths: {
        //  require specific plugins
        'require-domready'  : 'libs/requirejs-domready/domReady.min',
        'require-text'      : 'libs/requirejs-text/text.min',

        //  angular plugins
        'angular'           : 'libs/angular/angular.min',
        'angular-animate'   : 'libs/angular-animate/angular-animate.min',
        'angular-aria'      : 'libs/angular-aria/angular-aria.min',
        'angular-material'  : 'libs/angular-material/angular-material.min',
        'angular-ui-router' : 'libs/angular-ui-router/release/angular-ui-router.min',

        'angular-mocks'     : 'libs/angular-mocks/angular-mocks.min',

        //  lazy loading
        'ocLazyLoad'        : 'libs/oclazyload/dist/ocLazyLoad.min'
    },

    shim: {
        'angular' : {
            exports : 'angular'
        },
        'angular-aria' : {
            deps: ['angular'],
            exports : 'angular'
        },
        'angular-animate' : {
            deps: ['angular'],
            exports : 'angular'
        },
        'angular-material' : {
            deps: ['angular'],
            exports : 'angular'
        },
        'angular-mocks' : {
            deps: ['angular'],
            exports : 'angular'
        },
        'angular-ui-router' : {
            deps: ['angular'],
            exports : 'angular'
        },
        'ocLazyLoad' : {
            deps: ['angular'],
            exports :'angular'
        }
    }

    //waitSeconds:1,

/*    deps: [
        // kick start application... see bootstrap.js
        './require-bootstrap'
    ]*/
});

requirejs.onError = function(err)
{
    console.log(err);
    console.log(err.requireType);
    if(err.requireType === 'timeout')
    {
        console.log('modules: ' + err.requireModules);
    }
    throw err;
};

//  typescript definition for angular modules
declare module 'angular-animate' {}
declare module 'angular-aria' {}
declare module 'angular-material' {}
declare module 'angular-mocks' {}
declare module 'angular-ui-router' {}
declare module 'ocLazyLoad' {}

define(
    [
        'require',
        'angular',
        'app-bootstrap',

        'require-text',

        'app-module'
    ],
    function (require: Require, angular: ng.IAngularStatic, app : ng.IModule)
    {
        return require
        (
            ['require-domready!'],
            function(document)
            {
                //  this is resetting the window name deferring the angular bootstrap
                //window.name = '';
                angular.bootstrap(document, ['app']);
            }
        )

    }
);
